#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include "headers.h"
#define TAILLE_MOT_MAX 25
#define NB_DE_CHANCES 10
#define NB_JOUEURS_INTITIAL 1// 1 ou 2


int main(int argc, char* argv[])
{
    int premierTour = 0;
    int nbJoueurs = NB_JOUEURS_INTITIAL;
    while (1)
    {
        nbJoueurs = rejouer(nbJoueurs, premierTour);
        premierTour++;
        char mot[TAILLE_MOT_MAX];
        if(nbJoueurs == 1)
        {
            FILE* fichier = NULL;
            fichier = fopen("dictionnaire_pendu.timon", "r");
            if (fichier != NULL && fgetc(fichier) != EOF)
            {            
                rewind(fichier);
                srand(time(NULL));
                int nbLignes = nb_de_lignes();
                int hasard = rand()%(nbLignes+1);
                int m;
                for(m=hasard; m>0; m--)             //autant de fois que le nombre aléatoire
                {
                    int caractere = 0;
                    while(caractere != 10)        //saute une ligne
                    {
                        caractere = fgetc(fichier);
                    }
                }
                fgets(mot, TAILLE_MOT_MAX, fichier);// met le mot dans "mot"
                if (mot[strlen(mot)-1] == '\n') 
                {
                    mot[strlen(mot)-1] = '\0';
                }           //pour enlever le \n a la fin de chaque mot
            }
            else
            {
                printf("erreur dictionnaire");
                exit(1);
            }
            fclose(fichier);
            // ↑ lecture dans le fichier
        }
        if (nbJoueurs == 2)
        {
            printf("1er joueur : saisissez un mot a deviner   ");
            scanf ("%s",mot);
            while (getchar() != '\n');
        }

        char etoiles[TAILLE_MOT_MAX];
        int bonnesLettres = 0;
        int i;
        for (i = 0 ; i < strlen(mot) ; i++)
        {
            mot[i] = toupper(mot[i]);//met les lettres en majuscule
            if(mot[i] == '-')
            {
                etoiles[i] = '-';
                bonnesLettres ++;
            }
            else if (mot[i] == ' ')
            {
                etoiles[i] = ' ';
                bonnesLettres ++;
            }
            else if (mot[i] == '\'')
            {
                etoiles[i] = '\'';
                bonnesLettres ++;
            }
            else{etoiles[i] = '_';}
        }
        int chances = NB_DE_CHANCES;
        int victoire = 0;
        char lettresDites[500] = {'*'};
        int position = 0;
        while ((chances>0) && (victoire==0))
        {
            afficher_etoiles(strlen(mot), etoiles);
            int rate = 1;
            int dejaDit = 0;
            printf("\n");
            if (nbJoueurs == 2)
            {
                printf("2e joueur : ");
            }
            printf("saisissez une lettre ");
            char lettre = lireLettre();                                 
            int j;
            for (j = 0 ; j < position ; j++)
            {
                if (lettresDites[j] == lettre)
                {
                    printf("deja dit\n");
                    dejaDit = 1;
                }
            }
            if (dejaDit == 0)
            {
                lettresDites[position] = lettre; //empile toutes les lettres dites
                position++;
                int k;
                for (k = 0; k < strlen(mot); k++)
                {
                    if (lettre == mot[k])
                    {
                        etoiles[k] = lettre;
                        bonnesLettres ++;
                        rate = 0;
                    }
                }
                if (rate)
                {
                    chances --;
                    //printf("plus que %d chances!\n",chances);
                    afficher_pendu(chances);
                    
                }
                if (bonnesLettres >= strlen(mot))
                {
                    victoire = 1;
                }
            }
        }
        afficher_gp(victoire, mot, etoiles);
    }
    return 0;
}

//lit le mot jusqu'a entrée
char lireLettre()       
{

    char caractere = 0;
    caractere = getchar();
    if (caractere==10){caractere = getchar();}//sinon il lit les \n au 1er essai notemment
    caractere = toupper(caractere);
    while (getchar() != '\n');
    return caractere;
}

//affiche gagné ou perdu
void afficher_gp (int victoire, char* mot, char* etoiles)       
{
  afficher_etoiles(strlen(mot), etoiles);
  if (victoire)
    {
        printf("\n\nGAGNE\n");
    }
    else
    {
        printf("\n\nPERDU\n");
        printf("\nle mot etait %s\n",mot);
    }  
}

//affiche les tirets ( ou les , - )
void afficher_etoiles (int longueur,char* etoiles)      
{
    int l;
    printf("\n");
    for(l = 0; l < longueur; l++)
    {
        printf("%c ",etoiles[l]);
    }
}

//compte juste le nombre de lignes dans le dictionnaire
int nb_de_lignes()      
{
    int nb = 0;
    char lettre = 0;
    FILE* fichier = NULL;
    fichier = fopen("dictionnaire_pendu.timon", "r+");
    while (lettre != EOF)
        {
            lettre = fgetc(fichier); 
            if (lettre == '\n')
            {
                nb++;
            }
        }  
    fclose(fichier);
    return nb+2;
}

//affiche le dessin du pendu
void afficher_pendu(int pendu)      
{
    switch (pendu)
    {
    case 0:
        printf("    ___\n   |   |\n   |   O\n   |  _|_\n   |   |\n   |  / \\\n___|___\n");
        break;
    case 1:
        printf("    ___\n   |   |\n   |   O\n   |  _|_\n   |   |\n   |  /  \n___|___\n");
        break;
    case 2:
        printf("    ___\n   |   |\n   |   O\n   |  _|_\n   |   |\n   |    \n___|___\n");
        break;
    case 3:
        printf("    ___\n   |   |\n   |   O\n   |  _| \n   |   |\n   |    \n___|___\n");
        break;
    case 4:
        printf("    ___\n   |   |\n   |   O\n   |   | \n   |   |\n   |    \n___|___\n");
        break;
    case 5:
        printf("    ___\n   |   |\n   |   O\n   |     \n   |    \n   |    \n___|___\n");
        break;
    case 6:
        printf("    ___\n   |   |\n   |    \n   |     \n   |    \n   |    \n___|___\n");
        break;
    case 7:
        printf("    ___\n   |    \n   |    \n   |     \n   |    \n   |    \n___|___\n");
        break;
    case 8:
        printf("       \n   |    \n   |    \n   |     \n   |    \n   |    \n___|___\n");
        break;
    case 9:
        printf("       \n        \n        \n         \n        \n        \n_______\n");
        break;
    default:
        printf("erreur dessin pendu");
        break;
    }
}

//affiche le menu
int rejouer(int nbJoueurs, int premierTour)     
{
    int rejouer = 0;
    do 
    {
        if (premierTour == 0)//0 si c'est le 1er tour
        {
            printf("\n1 : jouer");
        }
        else
        {
            printf("\n1 : rejouer");
        }
        printf("\n2 : quitter\n3 : afficher les regles\n4 : passer en mode ");
        if (nbJoueurs == 1)
        {
            printf("2 joueurs\n\n");
        }
        else if (nbJoueurs == 2)
        {
            printf("1 joueur\n\n");
        }
        scanf("%d",&rejouer);
        if (rejouer == 3)
        {
            printf("       *LES REGLES*      \nCe sont les regles du pendu, 10 essais, tout est en majuscule automatiquement\n 1 joueur : pas d'accent, pas de cedille, pas de oe ni de ae, espaces , tirets et apostrophes possibles mais visibles. Les mots sont dans \"dictionnaire_pendu.timon\" ( aucune logique ) \n 2 joueurs : le premier joueur choisit le mot ( tous les caracteres sont acceptes ). Seul le mot avant 'espace' ou 'entree' est compte.\n");
        }
        else if (rejouer == 4)
        {
            nbJoueurs = !(nbJoueurs-1) + 1; //1->2, 2->1
        }
        else if (rejouer == 1)
        {
            return nbJoueurs;
        }
        else
        {
            exit(0);
        }
        
        
    }while (rejouer == 3 || rejouer == 4);
    return 0;
}


